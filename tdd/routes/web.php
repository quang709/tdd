<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::put('/task/update/{id}',[\App\Http\Controllers\TaskController::class,'update'])->name('tasks.update');
Route::get('/task/show/{id}',[\App\Http\Controllers\TaskController::class,'show'])->name('tasks.show');
Route::get('task/index',[\App\Http\Controllers\TaskController::class,'index'])->name('tasks.index');
Route::post('/task/store',[\App\Http\Controllers\TaskController::class,'store'])->name('tasks.store');
Route::delete('/task/destroy/{id}',[\App\Http\Controllers\TaskController::class,'destroy'])->name('tasks.destroy');
