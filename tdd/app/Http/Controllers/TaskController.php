<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskResquest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Utils;

class TaskController extends Controller
{
    protected $task;

    /**
     * @param $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }
    public function index()
    {
        $task = Task::all();
        return view('index',compact('task'));

    }
    public function store(StoreTaskResquest $request)
    {
       $this->task->create($request->all());
       return response()->json([],Response::HTTP_CREATED);


    }

    public function update(UpdateTaskRequest $request,$id)
    {
        $task = $this->task->findOrFail($id);
        $task->update($request->all());
//        return response()->json([],Response::HTTP_OK);
        return redirect(route('tasks.index'));
    }
    public function show($id)
    {
        $task = $this->task->findOrFail($id);

    }
    public function  destroy($id)
    {
        $task = $this->task->findOrFail($id);
        $task->delete();

    }
}
