<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{

     public function GetUpdateTaskRoute($id)
     {
         return route('tasks.update',$id);
     }
    public function GetRediTaskRoute()
    {
        return route('tasks.index');
    }
    /** @test  */
    public function user_can_update_task_if_task_validate()
    {
        $task = Task::factory()->create();
        $data = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];

        $response = $this->put($this->GetUpdateTaskRoute($task->id), $data);
//        $taskafter = Task::select('name', 'content')->where('id', $task->id)->first();
//        $this->assertEquals($data, $taskafter->toArray());
        $tasks = Task::find($task->id);
        $this->assertSame($tasks->name,$data['name']);
        $this->assertSame($tasks->content,$data['content']);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->GetRediTaskRoute());

    }
    /** @test  */
    public function user_can_not_update_task_if_task_name_is_null()
    {
        $task = Task::factory()->create();
        $data = [
            'name' => '',
            'content' => $this->faker->text
        ];

        $response = $this->put($this->GetUpdateTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
    /** @test  */
    public function user_can_not_update_task_if_task_content_is_null()
    {
        $task = Task::factory()->create();
        $data = [
            'name' => $this->faker->name,
            'content' =>  ''
        ];

        $response = $this->put($this->GetUpdateTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content']);
    }

    /** @test  */
    public function user_can_not_update_task_if_task_data_is_null()
    {
        $task = Task::factory()->create();
        $data = [
            'name' => '',
            'content' =>  ''
        ];

        $response = $this->put($this->GetUpdateTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content','name']);
    }
    /** @test  */
    public function user_can_not_update_if_task_not_exist()
    {
        $task = Task::factory()->make()->toArray();
        $id = -1;
        $response = $this ->put($this->GetUpdateTaskRoute($id),$task);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
