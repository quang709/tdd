<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListTaskTest extends TestCase
{
    public function getListTaskTest()
    {
        return route('tasks.index');
    }
    /** @test  */

    public function user_can_get_list_task()
    {
       $task = Task::factory()->create();

        $response = $this->get($this->getListTaskTest());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($task->name);
    }
}
