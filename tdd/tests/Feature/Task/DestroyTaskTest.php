<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Http\Response;
use Tests\TestCase;

class DestroyTaskTest extends TestCase
{

    public function getDestroyTaskRoute($id)
    {
        return route('tasks.destroy',$id);
    }
    /** @test  */

    public function user_can_destroy_task()
    {
        $task =Task::factory()->create();
        $taskbefore = Task::count();
        $response = $this->delete($this->getDestroyTaskRoute($task->id));
        $taskafter =Task::count();
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals($taskbefore-1,$taskafter);
        $this->assertDatabaseMissing('tasks',$task->toArray());
    }
    /** @test  */

    public function user_can_not_destroy_task()
    {
        $id =-1;
        $response = $this->delete($this->getDestroyTaskRoute($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
