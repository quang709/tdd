<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class StoreTaskTest extends TestCase
{

 public function getStoreTaskRoute()
 {
     return route('tasks.store');
 }
    /** @test  */
    public function user_can_store_task_is_validate()
    {
        $task = Task::factory()->make()->toArray();
        $taskbefore = Task::count();
        $response = $this->post($this->getStoreTaskRoute(), $task);
        $taskafter = Task::count();
        $this->assertEquals($taskbefore + 1, $taskafter);
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertDatabaseHas('tasks', $task);

    }
    /** @test  */
    public function user_can_not_store_task_if_name_is_null()
    {
        $task = Task::factory()->make(['name'=>'','content'=>$this->faker->text]);

        $response = $this->post($this->getStoreTaskRoute(),$task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
    /** @test  */
    public function user_can_not_store_task_if_content_is_null()
    {
        $task = Task::factory()->make(['name'=>$this->faker->name,'content'=>'']);

        $response = $this->post($this->getStoreTaskRoute(),$task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content']);
    }
    /** @test  */
    public function user_can_not_store_task_if_data_is_null()
    {
        $task = Task::factory()->make(['name'=>'','content'=>'']);

        $response = $this->post($this->getStoreTaskRoute(),$task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','content']);
    }

}
