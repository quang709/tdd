<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowTaskTesk extends TestCase
{
    public function GetShowTaskRoute($id)
    {
        return route('tasks.show',$id);
    }

    /** @test  */
    public function user_can_show_task()
    {

        $task = Task::factory()->create();

        $response = $this->get($this->GetShowTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
    }
    /** @test  */
    public function user_can_not_show_task()
    {

        $id = -1;

        $response = $this->get($this->GetShowTaskRoute($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
